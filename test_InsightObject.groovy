/**
* A set of tests on the InsightObject.groovy file
*
* Setup is included in the README.md file
*/

import InsightObject;
import groovy.transform.Field;

@Field final String SCHEMA = "Insight_Test";

/**
* Set up the schema by deleting any existing objects and creating new objects. This will do significant testing on its own
* 
* @return the list of new objects that are created
*/
List<InsightObject> initialize_tests() {
    List<InsightObject> object_list = InsightObject.IQL("", SCHEMA);
    object_list.each {
        log.warn("Deleting ${it}");
        assert it.deleteObject();
    }

    List<InsightObject> new_objects = [];

    ['Primary 1', 'Primary 2', 'Primary 3'].each {
        InsightObject.createObject(SCHEMA, "Primary", ['Name':it]);
    }

    [['Child 1', 'Primary 1'], ['Child 2', 'Primary 2'], ['Child 3', 'Primary 3']].each {
        InsightObject.createObject(SCHEMA, "Child", ['Name':it[0], 'Primary':it[1]]);
    }
    return InsightObject.IQL("", SCHEMA);
}

/**
* Test updating and getting text
*/
def test_text(InsightObject bean) {
    final String text1 = "This is text 1";
    final String text2 = "Text 2";
    bean.update("Text1", text1);
    assert (String) bean.get("Text1") == text1;

    bean.update("Text1", text2);
    assert (String) bean.get("Text1") == text2;
}

/**
* Test the Add and Retrieve comment capabilities
*/
def test_comments(InsightObject bean) {
    final String comment = "This is a test comment";
	log.warn(bean.objectBean);
    bean.addComment(comment);
    assert bean.getComments()[0] == comment;
}

List<InsightObject> object_list = initialize_tests();
InsightObject bean1 = object_list.find {it.get("Name") == "Primary 1"}
assert bean1;

test_text(bean1);
test_comments(bean1);


