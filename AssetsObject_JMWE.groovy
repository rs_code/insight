/**
* A Wrapper class for AssetsObjects
*
* Author: Derek Fields, Righstar
* Last Modified: 12/26/2024
* Version: 1.7
*
* Change Log:
*    11/23/2020: Added header information
*    6/10/2022: Modified "get" so that it returns an InsightObject for Referenced Objects
*    7/12/2022: Updated "createObject" to handle different data types
*    7/19/2022: Added code to "update" to handle List<ObjectBean> values
*   11/2/2022: Added "deleteAttribute" to allow the attribute values to be deleted. Use this, not a null or empty list
*    7/3/2023: Modified update to handle an empty list by deleting the values in the attribute
*    7/22/2024: Updated createObject to default to [value]
*
* Dependencies:
*    Assets
*    JMWE
*/

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.user.ApplicationUser;

// Insight imports;
import com.riadalabs.jira.plugins.insight.channel.external.api.facade.ObjectSchemaFacade;
import com.riadalabs.jira.plugins.insight.channel.external.api.facade.ObjectTypeFacade;
import com.riadalabs.jira.plugins.insight.channel.external.api.facade.ObjectTypeAttributeFacade;
import com.riadalabs.jira.plugins.insight.channel.external.api.facade.ObjectFacade;
import com.riadalabs.jira.plugins.insight.channel.external.api.facade.IQLFacade;
import com.riadalabs.jira.plugins.insight.services.model.ObjectBean;
import com.riadalabs.jira.plugins.insight.services.model.CommentBean;
import com.riadalabs.jira.plugins.insight.services.model.ObjectTypeBean;
import com.riadalabs.jira.plugins.insight.services.model.ObjectSchemaBean;
import com.riadalabs.jira.plugins.insight.services.model.ObjectTypeAttributeBean;
import com.riadalabs.jira.plugins.insight.channel.external.api.facade.IQLFacade;
import com.riadalabs.jira.plugins.insight.services.model.factory.ObjectAttributeBeanFactory;
import com.riadalabs.jira.plugins.insight.services.model.ObjectAttributeBean;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

class AssetsObject {
    private static final ObjectFacade of = ComponentAccessor.getOSGiComponentInstanceOfType(ObjectFacade);
    private static final ObjectSchemaFacade osf = ComponentAccessor.getOSGiComponentInstanceOfType(ObjectSchemaFacade);
    private static final ObjectTypeFacade otf = ComponentAccessor.getOSGiComponentInstanceOfType(ObjectTypeFacade);
    private static final ObjectTypeAttributeFacade otaf = ComponentAccessor.getOSGiComponentInstanceOfType(ObjectTypeAttributeFacade);
    private static final IQLFacade iqlFacade = ComponentAccessor.getOSGiComponentInstanceOfType(IQLFacade);
    private static final ObjectAttributeBeanFactory oabf = ComponentAccessor.getOSGiComponentInstanceOfType(ObjectAttributeBeanFactory);

    private ObjectSchemaBean objSchema;
    private ObjectTypeBean objectType;
    private ObjectBean objectBean;
    private Map objectValues = [:];
    private String objectKey;
    private Boolean isValid = true;
    private String errorMessage = "";
    private static final Logger log = LogManager.getLogger("com.rightstar.AssetsObject");

  def main(String[] a) {
    
  }
  
    /**
    * Returns a partial object. Does not retrieve any actual object from the database. Of limited use
    *
    * @param schema The name of the Insight schema
    * @param objectType The type of Insight object this will represent
    */
    AssetsObject(String schema, String objectType) {
        this.setSchema(schema);
        this.setObjectType(objectType);
    }

    /**
    * Returns an AssetsObject based on the Insight Object Bean passed in
    *
    * @param bean The Insight Object bean to wrap with this object
    */
    AssetsObject(ObjectBean bean) {
        objectBean = bean;
        if (isValid()) {
            setObjectTypeFromBean();
            if (isValid()) {
                setSchemaFromBean();
            }
        }
        if (!isValid()) {
            log.error(errorMessage);
        }
    }

    /**
    * Returns an AssetsObject based on the Insight Object Bean id
    *
    * @param id The integer id of the Insight Object Bean
    */
    AssetsObject(int id) {
        this.getObject(id);
        if (isValid()) {
            setObjectTypeFromBean();
            if (isValid()) {
                setSchemaFromBean();
            }
        }
    }
    
    /**
    * Returns an AssetsObject based on the Insight Object Bean key 
    *
    * @param key The key of the Insight Object (e.g., AS-1111)
    */
    AssetsObject(String key) {
        this.getObject(key);
        if (isValid()) {
            setObjectTypeFromBean();
            if (isValid()) {
                setSchemaFromBean();
            }
        }
        if (!isValid()) {
            log.error(errorMessage);
        }
    }

    /**
    * Fetch an Insight Object from the database based on its id
    *
    * @param    id  The integer id of the Insight Object
    */
    private ObjectBean getObject(int id) {
        objectBean = of.loadObjectBean(id);
        if (objectBean == null) {
            errorMessage = "getObject: Unable to find object with id $id";
            isValid = false;
        }
        return objectBean;
    }

    /**
    * Fetch an Insight Object from the database based on its key
    *
    * @param key The key of the Insight Object
    */
    private ObjectBean getObject(String key) {
        objectBean = of.loadObjectBean(key);
        if (objectBean == null) {
            errorMessage = "getObject: Unable to find object with key $key";
            isValid = false;
        }

        return objectBean;
    }

    /**
    * Set the Schema for retrieving this object
    *
    * @param schema The schema where this object lives
    */
    def setSchema(String schema) {
        objSchema = osf.findObjectSchemaBeans().find {it.name.equals(schema)}

        if (objSchema == null) {
            errorMessage = "setSchema: Unable to locate schema $schema";
            log.error(errorMessage);
            isValid = false;
        }
        return this.objSchema;
    }

    /**
    * Set the schema for this object by interrogating its Insight Object Bean
    *
    */
    private def setSchemaFromBean() {
        if (!isValid() || objectType == null) {
            errorMessage = "setSchemaFromBean: invalid Object";
            log.error(errorMessage);
            return;
        }

        objSchema = osf.loadObjectSchema(objectType.getObjectSchemaId());
        if (objSchema == null) {
            errorMessage = "setSchemaFromBean: Unable to load schema for id ${objectType.getObjectSchemaId()}";
            log.error(errorMessage);
            isValid = false;
        }
    }

    /**
    * get the object type from the Insight Object Bean
    *
    */
    private def setObjectTypeFromBean() {
        if (!isValid() || objectBean == null) {
            errorMessage = "setObjectTypeFromBean: invalid Object";
            log.error(errorMessage);
            return;
        }

        objectType = otf.loadObjectType(objectBean.getObjectTypeId());
        if (objectType == null) {
            errorMessage = "setObjectTypeFromBean: Unable to load object type bean for ${objectBean.getObjectTypeId()}";
            log.error(errorMessage);
            isValid = false;
        }
    }

    /**
    * Get the Schema associated with this Object
    */
    ObjectSchemaBean getSchema() {
        return this.objSchema;
    }

    /**
    * sets the object type corresponding to this object
    *
    * @param type The Object Type for this object
    */
    ObjectTypeBean setObjectType(String type) {
        if (! isValid()) {
            return;
        }
        objectType = otf.findObjectTypeBeans(objSchema.id).find {it.name.equals(type)}

        if (objectType == null) {
            errorMessage = "setObjectType: Unable to locate object type $type";
            isValid = false;
        }

        return objectType;
    }

    /**
    * Get the Object Type Bean
    */
    ObjectTypeBean getObjectType() {
        return this.objectType;
    }

    /*
    ** Return the Object Type as a String
    **/
    String getObjectTypeAsString() {
        return this.objectType.getName();
    }

	/**
	  Get the Object Key for this object
	**/
	String getKey() {
	  return this.getObjectBean().getObjectKey();
	}
	
    /**
    * Get the underlying ObjectBean for this Object
    */
    ObjectBean getObjectBean() {
        return this.objectBean;
    }

    /**
    * Return a map of the attributes for this bean. The attribute name is the key. The values are either a single value or a list of values
    *
    * @return Map of attribute values
    */
    Map<String, Object> getValues() {
        if (!isValid() || objectBean == null) {
            errorMessage = "getValues: Invalid Object";
            return;
        }

        Map<String, Object> objectValues = [:];

        objectBean.getObjectAttributeBeans().each { attr ->;
            def attrObjTypeId = attr.getObjectTypeAttributeId();
            def key = otaf.loadObjectTypeAttribute(attrObjTypeId)?.getName();
            objectValues[key] = this.get(key);
        }
        return objectValues;
    }

    /**
    * Return the value of a specific attribute for this object. The returned Object depends on the Attribute type. 
    *
    * @param key The name of the attribute to be returned
    * @return The value of the attribute. A single value is returned directly. Multiple values are returned as a list. If there is no value, then returns null
    */
    Object get(String key) {
        errorMessage = "";
        def value;
        List value_list = [];

        if (!isValid() || objectBean == null) {
            errorMessage = "get: Invalid Object";
            return null;
        }

        ObjectTypeAttributeBean attrType = otaf.findObjectTypeAttributeBeans(objectBean.getObjectTypeId()).find {it.name == key}
        if (attrType == null) {
            errorMessage = "get: Unable to find attribute type with name $key";
            return null;
        }

        def attrBean = objectBean.getObjectAttributeBeans().find {it.getObjectTypeAttributeId() == attrType.id}
        if (attrBean == null) {
            errorMessage = "get: Unable to find attribute $key for bean $objectBean}";
            return null;
        }

        def values = attrBean.getObjectAttributeValueBeans();
        if (values.size() == 0) {
            return null;
        } else if (values.size() == 1) {
            value = _transform(attrType, values[0].getValue());
        } else {
            values.each {
                value_list.add(_transform(attrType, it.getValue()));
            }
        }

        return (value != null ? value: value_list);
    }

    /**
    * Transforms a Referenced Object into an AssetsObject
    *
    * @param    attrType    The Attribute Type associated with this Object
    * @param    value       The Object that is being transformed
    * @return   The original Object or an AssetsObject if this is a reference to another ObjectBean
    */
    private Object _transform(ObjectTypeAttributeBean attrType, Object value) {
        if (attrType.isObjectReference()) {
            String schema = getSchema()?.getObjectSchemaKey();
            return new AssetsObject("$schema-$value");
        } else {
            return value;
        }
    }

    /**
    * Indicates whether this AssetsObject object is valid and points to an Insight Object
    *
    */
    Boolean isValid() {
        return objectBean != null;
    }

    /**
    * Returns the current error message. This will be populated if other calls fail
    */
    String getErrorMessage() {
        return this.errorMessage;
    }

    /*
    * Updates the current attribute value in the object.
    * If the value passed in is null, then it will call deleteAttribute
    * NOTE: This will not invalidate the cache and the next "get" on this same object will return the old attribute value
    *   The workaround is to instantiate a new version of the object, which will read in the data again.
    *
    * @param key The attribute name
    * @param value The value to be assigned to the attribute
    * @return true indicates that the update succeeded. false otherwise
    */
    Boolean update(String key, Object value) {
        def attrType = otaf.findObjectTypeAttributeBeans(this.getObjectType().getId()).find{it.name.equals(key)}
        if (attrType == null) {
            log.error("Unable to find attribute type $key");
        } else {
            List value_list = [];
            if (value == null || value == []) {
                return deleteAttribute(key);
            } else if (value.class != [].class) {
                value_list.add(value.toString());
            } else if (((List) value)?.get(0)?.class == ObjectBean) {
                value_list = ((List<ObjectBean>) value).collect {
                    it.getObjectKey();
                }
            } else {
                value_list = ((List) value).collect {
                    it.toString();
                }
            }
            def newAttribute = oabf.createObjectAttributeBeanForObject(this.objectBean, attrType, value_list as String[]);
            def oldAttribute = of.loadObjectAttributeBean(this.objectBean.getId(), attrType.getId());
            if (oldAttribute) {
                newAttribute.setId(oldAttribute.getId());
            }
            try {
                of.storeObjectAttributeBean(newAttribute);
                return true;
            } catch (Exception vie) {
                log.error("Unable to update object attribute $key with $value due to validation error: " + vie.getMessage());
                return false;
            }
        }
    }

    /**
    * Add a comment to the current object
    *
    * @param comment The comment to be added to the current object
    */
    def addComment(String comment) {
        errorMessage = "";

        if (!isValid() || objectBean == null) {
            errorMessage = "get: Invalid Object";
            log.error(errorMessage);
            return null;
        }

        CommentBean commentBean = new CommentBean();
        commentBean.setComment(comment);
        commentBean.setRole(0)  // Public;
        commentBean.setAuthor(ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser().getKey());
        commentBean.setObjectId(objectBean.getId());
        of.storeCommentBean(commentBean);
    }
    
    /**
    * Return the list of comments on this object
    *
    * @return List of comments as strings. Currently only the comment text, not the author or date are returned
    */
    List<String> getComments() {
        return of.findCommentBeans(objectBean.id).collect {
            it.getComment();
        }
    }

    /**
    * Delete the current object from the Insight database
    *
    * @return Boolean with true indicating success and false indicating failure.
    */
    Boolean deleteObject() {
        try {
            of.deleteObjectBean(objectBean.id);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    /**
    * Delete an attribute value from this Object
    *
    */
    def deleteAttribute(String attribute) {
        if (!isValid() || objectBean == null) {
            errorMessage = "deleteAttributes: Invalid Object";
            return; 
        }

        ObjectTypeAttributeBean attrType = otaf.findObjectTypeAttributeBeans(objectBean.getObjectTypeId()).find {it.name == attribute}
        if (attrType == null) {
            log.error("Unable to find attribute of type $attribute");
            return;
        }
        ObjectAttributeBean attr = objectBean.getObjectAttributeBeans().find { it.getObjectTypeAttributeId() == attrType.id }
        if (attr == null) {
            // log.warn("Attribute $attribute is already empty");
            return;
        }
        try {
            of.deleteObjectAttributeBean(attr.getId())
        } catch (Exception vie) {
            log.error("Unable to delete attribute $attribute: "+ vie.getMessage() );
        }
    }

    /**
    * Execute an IQL and return a list of AssetsObjects created from the IQL results. If there are no results, an empty list is returned
    *
    * @param iql The iql to be executed. No error checking is done on the IQL. This is handled by Insight and may result in an exception
    * @return A list (possibly empty) of the resulting objects
    */
    static List<AssetsObject> IQL(String iql, String schema = null) {
        if (schema == null) {
            return iqlFacade.findObjectsByIQL(iql).collect {
                new AssetsObject(it);
            }
        } else {
            ObjectSchemaBean schemaBean = osf.findObjectSchemaBeans().find {it.name.equals(schema)}

            if (schemaBean == null) {
                log.error("setSchema: Unable to locate schema $schema");
                return [];
            }
            return iqlFacade.findObjectsByIQLAndSchema(schemaBean.id, iql).collect {
                new AssetsObject(it);
            }
        }
    }

    /**
    * Create a new Insight Object
    *
    * @param schema The name of the schema for this new object
    * @param objectType The name of the object type to be created
    * @param attributes A map of attribute names and values to populate in the new object
    * @return An AssetsObject corresponding to the newly created Object Bean or null if creation failed. Check the log for failure messages
    */
    static AssetsObject createObject(String schema, String objectType, Map<String, Object> attributes) {
        if (schema == null || schema == "") {
            log.error("createObject: schema is null or empty");
            return null;
        }
        if (objectType == null || objectType == "") {
            log.error("createObject: objectType is null or empty");
            return null;
        }
        def objSchema = osf.findObjectSchemaBeans().find {it.name.equals(schema)}
        if (objSchema == null) {
            log.error("createObject: Unable to locate schema $schema");
            return null;
        }

        def objType = otf.findObjectTypeBeansFlat(objSchema.id).find {it.name.equals(objectType)}
        if (objType == null) {
            log.error("createObject: Unable to locate object type $objectType");
            return null;
        }

        ObjectBean newBean = objType.createMutableObjectBean();
        def objectAttributeBeans = new ArrayList();
        attributes.each {key, value ->;
            def attributeType = otaf.findObjectTypeAttributeBeans(objType.id).find{it.name.equalsIgnoreCase((String) key)}
            if (attributeType != null) {
                String[] valueList = [value]
                if (value == null) {
                    valueList == null
                } else if (value.class == "".class) {
                    valueList = [value]
                } else if (value.class == 1.class) {
                    valueList = [value.toString()]
                } else if (value.class == [].class) {
                    valueList = (List) value;
                }
                def attrBean = oabf.createObjectAttributeBeanForObject(newBean, attributeType, valueList);
                if (attrBean != null) {
                    objectAttributeBeans.add(attrBean);
                }
            }
        }
        
        newBean.setObjectAttributeBeans(objectAttributeBeans);
        ObjectBean anewBean;
        try {
            anewBean = of.storeObjectBean(newBean);
        } catch (Exception vie) {
            log.error("createObject: Unable to create ObjectBean" + vie.getMessage());
            return null;
        }
        return new AssetsObject(anewBean);
    }  
}