# Insight Repository

## Purpose

This repository provides a list of code snippets and Groovy classes that can easy Insight usage.

## Contents

### InsightObject.groovy 

This is a general purpose class that has both instantiated methods and static methods. It provides a wrapper for a standard Insight ObjectBean and allows quick access to attributes. It also provides static classes to get IQL results and to create a new ObjectBean.

The groovydoc documentation can be found at <file://.doc/index.html>

### insight-tool2.groovy

This is a replacement for the standard PDF Exporter groovy script. The only difference is that it returns a dictionary for the attributes where the original returned a list of dictionaries, which wasn't that useful.

### test_InsightObject.groovy

This is a test package to ensure that InsightObject.groovy continues to work after changes. If new capabilities are added or changes made to InsightObject.groovy, this test package should be modified and run.

It requires the following Insight Schema to be created in the test environment. All names are case sensitive.

* Schema 
    * Name: Insight_Test
    * Key: ITEST
* Object
    * Name: Primary
    * Additional Attributes: 
        * Text1, Default, Text
        * Float1, Default, Float
        * Integer1, Default, Integer
        * Date1, Default, Date
        * DateTime1, Default, DateTime
* Object
    * Name: Child
    * Attributes: Primary, Object, Primary, Dependency 

The schema should start empty. Any objects that already exist will be deleted and recreated.

## Submodule Usage
This repository is intended to be used as a submodule in other scripts that need Insight support.

### Commands for submodule support

#### Add the insight repo as a submodule
At the top level of your repository, execute

    git submodule add <repo url> insight

This will add this repository as a subdirectory called insight.

### Update the insight repo to get the most recent changes
This will not happen automatically. You need to make sure that you update from remote server

    cd insight
    git pull
    cd ..
    git commit -am "Pulled down update to insight submodule"

This will return you to the root directory having updated the insight code with a commit point.

