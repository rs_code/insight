import com.atlassian.jira.component.ComponentAccessor
import org.apache.log4j.Logger

insight = new InsightTool()

/**
 * Velocity tool to get Insight object data.
 */
class InsightTool {
    def log = Logger.getLogger(this.getClass())

    /* Insight API. */
    def insightObjectFacade
    def insightObjectTypeAttributeFacade

    def insightInitialized = false

    /**
     * Returns the attributes of the Insight object (as a map from attribute names to value collections).
     */
    def getAttributes(def object) {
        def attributes = [:]

        // lazily load dependencies (attempt only once)
        if(!insightInitialized) {
            try {
                def objectFacadeClass = ComponentAccessor.pluginAccessor.classLoader.findClass("com.riadalabs.jira.plugins.insight.channel.external.api.facade.ObjectFacade")
                insightObjectFacade = ComponentAccessor.getOSGiComponentInstanceOfType(objectFacadeClass)

                def objectTypeAttributeFacadeClass = ComponentAccessor.pluginAccessor.classLoader.findClass("com.riadalabs.jira.plugins.insight.channel.external.api.facade.ObjectTypeAttributeFacade")
                insightObjectTypeAttributeFacade = ComponentAccessor.getOSGiComponentInstanceOfType(objectTypeAttributeFacadeClass)
            } catch (Exception ex) {
                log.warn("Failed to find the Insight API", ex)
            }
            insightInitialized = true
        }

        if(insightObjectFacade && insightObjectTypeAttributeFacade) {
            object.objectAttributeBeans.each { objectAttribute ->
                if(objectAttribute.id != null) {
                    def attribute = insightObjectTypeAttributeFacade.loadObjectTypeAttributeBean(objectAttribute.objectTypeAttributeId)
                    if(attribute != null) {
                        def values = objectAttribute.objectAttributeValueBeans.collect { attributeValue ->
                            (attributeValue.referencedObjectBeanId == null) ? attributeValue.value.toString() : insightObjectFacade.loadObjectBean(attributeValue.referencedObjectBeanId)?.name
                        }
                        attributes[attribute.name]= values
                    }
                }
            }
        } else {
            log.warn("Insight API not initialized")
        }

        return attributes
    }
}

